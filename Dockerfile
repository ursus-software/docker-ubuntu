FROM ubuntu:14.04
MAINTAINER Djordje Stojanovic <djordje.stojanovic@ursus.rs>
LABEL vendor="Ursus Software"

RUN apt-get update -qq \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -qq -y git-core curl jq supervisor \
	&& apt-get clean -qq
	
RUN ln -s /etc/supervisor/supervisord.conf /etc/supervisor.conf
COPY supervisord.conf /etc/supervisor/supervisord.conf
